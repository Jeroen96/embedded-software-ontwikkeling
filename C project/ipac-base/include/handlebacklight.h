#ifndef _handlebacklight_H
#define _handlebacklight_H

/*
 *this function runs a background thread to handle the led backlight
 */
extern void startbackgroundhandling(void);

#endif
