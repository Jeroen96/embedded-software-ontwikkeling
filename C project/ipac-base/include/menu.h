#ifndef _MENU_H
#define _MENU_H

/*The main settings menu function. Loops forever till exit*/
extern void handleMenu(void);

//this function returns the utc value (only get)
extern int getUtc(void);

//this function sets the utc value, value must be between -12 and 12, otherwise its not possible
extern void setUtc(int utc);

#endif /* _MENU_H */
