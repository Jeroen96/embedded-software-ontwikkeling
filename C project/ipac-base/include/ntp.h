#ifndef _Ntp_H
#define _Ntp_H

typedef enum { false, true } bool;
extern int playing;

extern int NtpIsSyncing(void);
extern void NtpInit(void);
extern void GetTime(void);
extern void PlayStream(void);
extern void SetRadioSelect(int);
extern void SetPlaying(int);
extern int GetSelectedRadio(void);

#endif /* _Ntp_H */
