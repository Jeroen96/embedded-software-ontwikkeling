#ifndef _FILEIO_H
#define	_FILEIO_H

/*Struct with the user settings*/

struct USER_CONFIG
{
	unsigned int utc;
};

/*Loads the user_config struct attributes from the non-volatile memory*/
extern void loadUserConfig(void);	

/*Saves the user_config struct attributes to the non-volatile memory*/
extern void saveUserConfig(void);

struct USER_CONFIG userConfig;  /*MAKE SURE LOADUSERCONFIG IS CALLED BEFORE USING!!!!*/

#endif /*FILEIO_H_*/
