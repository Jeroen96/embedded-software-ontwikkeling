#ifndef _valueCheck_H
#define _valueCheck_H

extern int checkValue(int value, int max, int min);
extern int mutateValue(int value, int mutate, int max, int min);
extern void mutateValueWithF(int(*getValue)(void), void(*setValue)(int), int mutate, int max, int min);

#endif
