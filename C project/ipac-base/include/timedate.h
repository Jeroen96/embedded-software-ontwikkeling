#ifndef _timedate_H
#define _timedate_H

#include <time.h>           // for tm-struct

/*prototypes*/
void setTime(tm *tm);
void getTime(tm *tm);
int getMaxDaysOfMonth(tm *gmt);
void initCheckThread(void);

#endif
/*empty line*/
