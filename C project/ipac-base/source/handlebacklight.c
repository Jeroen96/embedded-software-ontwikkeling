#include <sys/thread.h>
#include <sys/timer.h>
#include "../include/x1205.h"
#include "../include/display.h"
#include "../include/led.h"
#include <time.h>
#include "../include/timedate.h"
#include "keyboard.h"


THREAD(HandlebackroundLight, arg){
	NutThreadSetPriority(100);
	tm gmt;
	int temp = 0;
	int tempv2 = 0;
	int i = 0;
	for(;;){
		getTime(&gmt);
		tempv2++;
		if (tempv2 > (temp + 100))
				LcdBackLight(LCD_BACKLIGHT_OFF);
			for (i = 0; i < 17; i++)
			{
				if(KbGetKey() == i)
				{
					LcdBackLight(LCD_BACKLIGHT_ON);
					temp = gmt.tm_sec;
					tempv2 = gmt.tm_sec;
				}
			}
	}
}

void startbackgroundhandling(void){
		NutThreadCreate("HPT", HandlebackroundLight, NULL, 512);
}
