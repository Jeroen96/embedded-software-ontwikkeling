#define LOG_MODULE  LOG_MAIN_MODULE

#include <stdio.h>
#include <sys/thread.h>
#include <time.h>           // for tm-struct
#include <sys/timer.h>
#include "timedate.h"
#include "rtc.h"
#include "log.h"


#define Check_Bit(var,pos) ((var)&(1<<(pos)))
#define Bit(x)(1<<x)

/*een 2d array, 
* omdat er een schrikkeljaar kan zijn, 
* dus moet er per maand worden aangegeven
*/
int daysofmonth[12][2]=
{
	{31,31},	//januari
	{28,29},	//februari
	{31,31},	//maart
	{30,30},	//april
	{31,31},	//mei
	{30,30},	//juni
	{31,31},	//juli
	{31,31},	//augustus
	{30,30},	//september
	{31,31},	//oktober
	{30,30},	//november
	{31,31},	//december
};


/*prototypes*/
void checkDataTime(tm *tm);

int lastmonthday = 0;

void setTime(tm *tm)
{
	int result = X12RtcSetClock(tm);
	lastmonthday = tm->tm_mday;	
	if(result == 0)
		printf("\n Time succesfully set in rtc");
	else
		printf("\n Failed to set time in rtc");
}

void getTime(tm *tm)
{
	X12RtcGetClock(tm);	
	if(lastmonthday != tm->tm_mday)
	{
		checkDataTime(tm);
		lastmonthday = tm->tm_mday;
	}
}

void checkDataTime(tm *tm)
{	
	int schrikkeljaar = (tm->tm_year % 4) == 0;
	
	if(tm->tm_mday > daysofmonth[tm->tm_mon-1][schrikkeljaar])
	{
		tm->tm_mday = 1;
		tm->tm_mon++;
		tm->tm_yday++;
		if(tm->tm_mon > 12)
		{
			tm->tm_mon = 1;
			tm->tm_yday = 1;
			tm->tm_year++;			
		}
	}
}

int getMaxDaysOfMonth(tm *gmt) 
{
	return daysofmonth[gmt->tm_mon-1][((gmt->tm_year % 4) == 0)];
}
