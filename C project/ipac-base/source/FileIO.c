#include "../include/FileIO.h"
#include <dev/board.h>
#include <stdio.h>
#include <io.h>
#include "../include/menu.h"
#include <dev/nvmem.h>

void loadUserConfig()
{
	NutNvMemLoad(256, &userConfig, sizeof(userConfig));
	setUtc(userConfig.utc);//Save utc according to utc in userConfig
	printf("utc loaded value: %d \n", userConfig.utc);
}

void saveUserConfig()
{
	//get utc and save from menu.h/getutc()
	userConfig.utc = getUtc();
	printf("utc saved value: %d \n", userConfig.utc);
	NutNvMemSave(256, &userConfig, sizeof(userConfig));
}


