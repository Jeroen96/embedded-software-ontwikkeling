#include <dev/board.h>
#include <sys/timer.h>
#include <sys/confnet.h>

#include <dev/nicrtl.h>

#include <stdio.h>
#include <io.h>
#include <arpa/inet.h>
#include <pro/dhcp.h>
#include "../include/network.h"
#include "../include/ntp.h"

#define rightIP "192.168.137.1"
int network = 0;

int GetNetwork(){
	return network;
}

void NetworkInit() {
	/* Register de internet controller. */
		if (NutRegisterDevice(&DEV_ETHER, 0, 0)) {
			printf("NO NETWORK: Registering  failed.");
			network = 0;
		}/* Netwerk configureren op dhcp */
		else if (NutDhcpIfConfig(DEV_ETHER_NAME, NULL, 0)) {
			/* Done. */
			printf("NO NETWORK: Error: Cannot configure ");
			network = 0;
		}
		else if (inet_ntoa(confnet.cdn_ip_addr) == rightIP) {
			printf("Ik heb een goede internet connectie. Ip is: %s \n", inet_ntoa(confnet.cdn_ip_addr));
			network = 1;
		}
		else {
			printf("ik heb slechte internet connectie Ip is: %s \n", inet_ntoa(confnet.cdn_ip_addr));
			network = 1;
	}
}


