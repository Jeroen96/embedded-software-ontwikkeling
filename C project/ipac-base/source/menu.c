#define LOG_MODULE  LOG_MAIN_MODULE

/*--------------------------------------------------------------------------*/
/*  Include files                                                           */
/*--------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>

#include <sys/thread.h>
#include <sys/timer.h>
#include <sys/version.h>
#include <dev/irqreg.h>

#include "../include/system.h"
#include "../include/portio.h"
#include "../include/display.h"
#include "../include/remcon.h"
#include "../include/keyboard.h"
#include "../include/led.h"
#include "../include/log.h"
#include "../include/uart0driver.h"
#include "../include/mmc.h"
#include "../include/watchdog.h"
#include "../include/flash.h"
#include "../include/spidrv.h"
#include "../include/x1205.h"
#include "../include/timedate.h"

#include <time.h>
#include "../include/rtc.h"
#include "../include/menu.h"
#include "../include/timedate.h"
#include "../include/ntp.h"
#include "../include/FileIO.h"

//##############################################################################################_Prototypes

//===========================================_Enums
typedef enum { Tijdzone, Tijd_Datum, Stream }Menu_Enums;
typedef enum { LINE1, LINE2 } LCD_LINE_ENUM;
typedef enum { Hours, Minutes, Days, Month, Year }Cursor_Enum;

//===========================================_Lcd_functions
void lcdPrintfPos(LCD_LINE_ENUM line, unsigned char col, char *s);
void lcdPrintfPosMan(unsigned char pos, char *s);
void lcdSetCursorPos(LCD_LINE_ENUM line, unsigned char col);
void lcdSetCursorPosMan(unsigned char pos);

//===========================================_Menukey_functions
/*1. Tijdzone functions*/

void upTijdzone(void);
void downTijdzone(void);
void okTijdzone(void);

/*2. Time & Date functions*/

void upTime_Date(void);
void downTime_Date(void);
void leftTime_Date(void);
void rightTime_Date(void);
void okTime_Date(void);

/*3. Stream*/

void upStream(void);
void downStream(void);
void okStream(void);

//===========================================_Menu_vars
/*1. Timezone vars*/
int utc; //Utc default value
int oldUtc; //Old utc value

//this function returns the utc value (only get)
int getUtc() {
	return utc;
}

/*
 * param must be between -12 and 12
 */
void setUtc(int utcparam) {
	utc = utcparam;
	printf("menu.h: utc set to: %d \n", utc);
}

/*2. Time & Date vars*/
tm gmt; //Time struct

/*3. Stream vars*/
int radioSelected; //only use this for the stream menu
char *radioStations[] = { "3FM","Radio6","Radio5" };

//===========================================_Menu_functions
/*Fill strings on first screenload(else strings are empty until button is pressed)*/
void init(void);

//===========================================_Edit functions & vars

int edit_enabled; //Flag to keep track of edit mode
void StartEditMode(Menu_Enums menu);//Start menu specific behaviour

/*Cursor location before screens gets repainted again.*/
unsigned char location;
Cursor_Enum Cursor_Current;//Current cursor location var

/*All menu struct strings
\Changed from 16 to 17 to fit /0 in string to fix memory overlapping*/
char m1_string[17]; //Timezone string
char m2_string[17]; //Time & Date string
char m3_string[17];//Steam string		

//===========================================_Menu template & array

/*Menu state template*/
typedef struct {
	Menu_Enums MenuID;	//Menu type
	void(*fp_onKeyLEFT)(void);  //LEFT-keypress funtion-pointer
	void(*fp_onKeyRIGHT)(void); //RIGHT-keypress funtion-pointer
	void(*fp_onKeyUP)(void);    //UP-keypress funtion-pointer
	void(*fp_onKeyDOWN)(void);  //DOWN-keypress funtion-pointer
	void(*fp_onKeyOk)(void);	//OK-Keypress function-pointer
	Menu_Enums newMenuIDKey[2]; //previous and next menu options
	char *txt[2]; // text on lcd_row 1 and 2 
}Menu_STATE;

/*Menu state array with all menu screens*/
Menu_STATE menu[] = {
	//1. Tijdzone-menu
	{
		Tijdzone,
		0L,
		0L,
		&upTijdzone,
		&downTijdzone,
		&okTijdzone,
		{ Tijdzone,Tijd_Datum },
		{ "1. Tijdzone     ",
		  m1_string }
	},
	//2. Tijd & Datum
	{
		Tijd_Datum,
		&leftTime_Date,
		&rightTime_Date,
		&upTime_Date,
		&downTime_Date,
		&okTime_Date,
		{ Tijdzone,Stream },
		{ "2. Tijd en Datum",
		  m2_string }
	},
	//3. Stream
	{
		Stream,
		0L,
		0L,
		&upStream,
		&downStream,
		&okStream,
		{ Tijd_Datum,Stream},
		{ "3. Stream",
		  m3_string }
	}
};

//##########################################################################################_Prototypes_END

/* ################################ Menu handling functions ################################ */

void handleMenu(void) {
	getTime(&gmt);
	LcdBackLight(LCD_BACKLIGHT_ON);
	unsigned int iItem = 0;
	unsigned int iText = 0;
	unsigned int menuloop_done = 0;
	unsigned int keyloop_done = 0;
	unsigned int menu_refresh = 1;
	oldUtc = utc;
	edit_enabled = 0;
	Menu_Enums currentmenu = Tijdzone;
	LogMsg_P(LOG_INFO, PSTR("Handle menu called"));
	init();

	while (!menuloop_done) {
		iItem = 0;
		/* Lookup current menuID in the static array of menu items */
		while (menu[iItem].MenuID != currentmenu) iItem++;

		/* Display the x lines of static text */
		if (menu_refresh)
		{
			LcdWriteByte(WRITE_COMMAND, 0x01);

			for (iText = 0; iText < 2; iText++)
			{
				if (menu[iItem].txt[iText] != NULL)
				{
					lcdPrintfPos(LINE_1 + iText, 0, menu[iItem].txt[iText]);
				}
			}
			menu_refresh = 0;
			LcdWriteByte(WRITE_COMMAND, location);//Set cursor back to old place(= cursor location)
		}

		/* Loop for key events and act according */
		keyloop_done = 0;
		u_char key = KbGetKey();

		switch (key)
		{
		case KEY_LEFT:
			if (*menu[iItem].fp_onKeyLEFT)
				(*menu[iItem].fp_onKeyLEFT)();
			/* Turn off menu navigation when editing */
			if (!edit_enabled) {
				currentmenu = menu[iItem].newMenuIDKey[0];
			}
			menu_refresh = 1;
			keyloop_done = 1;
			NutSleep(200);
			break;

		case KEY_RIGHT:
			if (*menu[iItem].fp_onKeyRIGHT)
				(*menu[iItem].fp_onKeyRIGHT)();
			/* Turn off menu navigation when editing or in sub menu screen */
			if (!edit_enabled) {
				currentmenu = menu[iItem].newMenuIDKey[1];
			}
			menu_refresh = 1;
			keyloop_done = 1;
			NutSleep(200);
			break;

		case KEY_UP:
			if (*menu[iItem].fp_onKeyUP)
				(*menu[iItem].fp_onKeyUP)();
			menu_refresh = 1;
			keyloop_done = 1;
			NutSleep(200);
			break;

		case KEY_DOWN:
			if (*menu[iItem].fp_onKeyDOWN)
				(*menu[iItem].fp_onKeyDOWN)();
			menu_refresh = 1;
			keyloop_done = 1;
			NutSleep(200);
			break;

		case KEY_ALT:
			edit_enabled = 1;
			LedControl(LED_ON);
			StartEditMode(currentmenu);
			NutSleep(200);
			break;

		case KEY_OK:
			if (*menu[iItem].fp_onKeyOk)
				(*menu[iItem].fp_onKeyOk)();
			edit_enabled = 0;
			menu_refresh = 1;
			LedControl(LED_OFF);
			LcdWriteByte(WRITE_COMMAND, 0x0C);//Make cursor invisible
			NutSleep(200);
			break;
		}
		/* lower sensitivity*/
		NutSleep(100);
	}
}

/* ################################ 1. Tijdzone key functions ################################ */

void upTijdzone(void)
{
	if (edit_enabled) {
		utc++;
		if (utc > 12)
			utc = 12;
		sprintf(m1_string, "UTC    %d    ", utc);
		LogMsg_P(LOG_INFO, PSTR("Utc: %d"), utc);
		LogMsg_P(LOG_INFO, PSTR("String: %s"), menu[0].txt[1]);
	}
}

void downTijdzone(void)
{
	if (edit_enabled) {
		utc--;
		if (utc < -12)
			utc = -12;
		sprintf(m1_string, "UTC    %d    ", utc);
		LogMsg_P(LOG_INFO, PSTR("Utc: %d"), utc);
	}
}

void okTijdzone(void) {
	if (edit_enabled) {
		saveUserConfig();
		getTime(&gmt);//Get current time

		if (oldUtc != utc) {
			gmt.tm_hour -= oldUtc; //Set utc time back to timezone 0
			gmt.tm_hour += utc; //Add selected timezone value to hours
		}
		oldUtc = utc;
		setTime(&gmt);
		LogMsg_P(LOG_INFO, PSTR("OkTijdzone called"));
	}


}

/* ################################ 2. Time & Date key functions ################################ */

void upTime_Date(void) {
	if (edit_enabled) {
		int maxdays = getMaxDaysOfMonth(&gmt);
		switch (Cursor_Current)
		{
		case Hours:
			gmt.tm_hour += 1;
			if (gmt.tm_hour >= 23)
				gmt.tm_hour = 23;
			break;
		case Minutes:
			gmt.tm_min += 1;
			if (gmt.tm_min >= 59)
				gmt.tm_min = 59;
			break;
		case Days:
			gmt.tm_mday += 1;
			if (gmt.tm_mday >= maxdays)
				gmt.tm_mday = maxdays;
			break;
		case Month:
			gmt.tm_mon += 1;
			if (gmt.tm_mon >= 12)
				gmt.tm_mon = 12;
			maxdays = getMaxDaysOfMonth(&gmt);
			if (gmt.tm_mday >= maxdays)
				gmt.tm_mday = maxdays;
			break;
		case Year:
			gmt.tm_year += 1;
			maxdays = getMaxDaysOfMonth(&gmt);
			if (gmt.tm_mday >= maxdays)
				gmt.tm_mday = maxdays;
			break;
		}
	}
	sprintf(m2_string, "%02d:%02d %02d-%02d-%d", gmt.tm_hour, gmt.tm_min, gmt.tm_mday, gmt.tm_mon, gmt.tm_year); //2.Time & Date 
}

void downTime_Date(void) {
	if (edit_enabled) {
		int maxdays;
		switch (Cursor_Current)
		{
		case Hours:
			gmt.tm_hour -= 1;
			if (gmt.tm_hour <= 0)
				gmt.tm_hour = 0;
			break;
		case Minutes:
			gmt.tm_min -= 1;
			if (gmt.tm_min <= 0)
				gmt.tm_min = 0;
			break;
		case Days:
			gmt.tm_mday -= 1;
			if (gmt.tm_mday <= 1)
				gmt.tm_mday = 1;
			break;
		case Month:
			gmt.tm_mon -= 1;
			if (gmt.tm_mon <= 1)
				gmt.tm_mon = 1;
			maxdays = getMaxDaysOfMonth(&gmt);
			if (gmt.tm_mday >= maxdays)
				gmt.tm_mday = maxdays;
			break;
		case Year:
			gmt.tm_year -= 1;
			maxdays = getMaxDaysOfMonth(&gmt);
			if (gmt.tm_mday >= maxdays)
				gmt.tm_mday = maxdays;
			break;
		}
	}
	sprintf(m2_string, "%02d:%02d %02d-%02d-%d", gmt.tm_hour, gmt.tm_min, gmt.tm_mday, gmt.tm_mon, gmt.tm_year); //2.Time & Date 
}


void leftTime_Date(void) {
	if (edit_enabled) {
		switch (Cursor_Current)
		{
		case Hours:
			//Do nothing
			break;
		case Minutes:
			LcdWriteByte(WRITE_COMMAND, 0xC1);
			location = 0xC1;
			Cursor_Current = Hours;
			break;
		case Days:
			LcdWriteByte(WRITE_COMMAND, 0xC4);
			location = 0xC4;
			Cursor_Current = Minutes;
			break;
		case Month:
			LcdWriteByte(WRITE_COMMAND, 0xC7);
			location = 0xC7;
			Cursor_Current = Days;
			break;
		case Year:
			LcdWriteByte(WRITE_COMMAND, 0xCA);
			location = 0xCA;
			Cursor_Current = Month;
			break;
		}
	}
}

void rightTime_Date(void) {
	if (edit_enabled) {
		switch (Cursor_Current)
		{
		case Hours:
			LcdWriteByte(WRITE_COMMAND, 0xC4);
			location = 0xC4;
			Cursor_Current = Minutes;
			break;
		case Minutes:
			LcdWriteByte(WRITE_COMMAND, 0xC7);
			location = 0xC7;
			Cursor_Current = Days;
			break;
		case Days:
			LcdWriteByte(WRITE_COMMAND, 0xCA);
			location = 0xCA;
			Cursor_Current = Month;
			break;
		case Month:
			LcdWriteByte(WRITE_COMMAND, 0xCF);
			location = 0xCF;
			Cursor_Current = Year;
			break;
		case Year:
			//Do nothing
			break;
		}
	}
}

void okTime_Date(void) {
	if (edit_enabled) {
		setTime(&gmt);
		printf("Time & Date saved...  value: %02d:%02d %02d-%02d-%d", gmt.tm_hour, gmt.tm_min, gmt.tm_mday, gmt.tm_mon, gmt.tm_year);
	}
}

/* ################################ 3. Stream key functions ################################ */

void upStream(void) {
	if (edit_enabled) {
		radioSelected += 1;
		if (radioSelected >= 2)
			radioSelected = 2;
		sprintf(m3_string, "%s", radioStations[radioSelected]);

		LogMsg_P(LOG_INFO, PSTR("radiostation %s"), radioStations[radioSelected]);
	}
}

void downStream(void) {
	if (edit_enabled) {
		radioSelected -= 1;
		if (radioSelected <= 0)
			radioSelected = 0;
		sprintf(m3_string, "%s", radioStations[radioSelected]);
	}
}

void okStream(void) {
	if (edit_enabled) {
		SetRadioSelect(radioSelected);
	}
	else {
		if (playing == 0)
			SetPlaying(1);
		else
			SetPlaying(0);
	}

}

/* ################################ LCD Functions ################################ */

void lcdPrintfPos(LCD_LINE_ENUM line, unsigned char col, char *s)
/*
* Input:
* Output:
* Description:
*
*******************************************************************/
{
	lcdSetCursorPos(line, col);

	while (*s != '\0')
	{
		LcdChar(*s);
		s++;
	}
}

void lcdSetCursorPos(LCD_LINE_ENUM line, unsigned char col)
{
	unsigned char ddram_addr = 0;

	switch (line)
	{
	case LINE1:
		ddram_addr = 0xC0;
		break;
	case LINE2:
		ddram_addr = 0x80;
		break;
	default:
		ddram_addr = 0xC0;
		break;
	}
	LcdWriteByte(WRITE_COMMAND, ddram_addr);
}

void lcdPrintfPosMan(unsigned char pos, char *s)
/*
* Input:
* Output:
* Description:
*
*******************************************************************/
{
	lcdSetCursorPosMan(pos);

	while (*s != '\0')
	{
		LcdChar(*s);
		s++;
	}
}

void lcdSetCursorPosMan(unsigned char pos)
{
	unsigned char ddram_addr = pos;
	LcdWriteByte(WRITE_COMMAND, ddram_addr);
}

/* ################################ Edit & init functions ################################ */

/*Init screen (otherwise lcd is empty till button press),only add string functions*/
void init(void) {
	/*1. Timezone*/
	sprintf(m1_string, "UTC    %d    ", utc); //1.Timezone

	/*2. Time & Date*/
	sprintf(m2_string, "%02d:%02d %02d-%02d-%d", gmt.tm_hour, gmt.tm_min, gmt.tm_mday, gmt.tm_mon, gmt.tm_year); //2.Time & Date 

	/*3. Stream*/
	sprintf(m3_string, "%s", radioStations[radioSelected]); // 3. Stream info
}

/*Start menu dependend behaviour when edit is started*/
void StartEditMode(Menu_Enums menu) {
	switch (menu)
	{
	case Tijdzone:
		LcdWriteByte(WRITE_COMMAND, 0x0C);//Make cursor invisible
		break;
	case Tijd_Datum:
		Cursor_Current = Hours;
		LcdWriteByte(WRITE_COMMAND, 0x0F);//Blinking cursor enabled
		LcdWriteByte(WRITE_COMMAND, 0xC1);//Start position 2 line 2
		location = 0xC1;
		break;
	case Stream:
		//Do nothing
		break;
	}
}

