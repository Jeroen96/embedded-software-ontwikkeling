/*! \mainpage SIR firmware documentation
 *
 *  \section intro Introduction
 *  A collection of HTML-files has been generated using the documentation in the sourcefiles to
 *  allow the developer to browse through the technical documentation of this project.
 *  \par
 *  \note these HTML files are automatically generated (using DoxyGen) and all modifications in the
 *  documentation should be done via the sourcefiles.
 */

/*! \file
 *  COPYRIGHT (C) STREAMIT BV 2010
 *  \date 19 december 2003
 */
 
 
 

#define LOG_MODULE  LOG_MAIN_MODULE

/*--------------------------------------------------------------------------*/
/*  Include files                                                           */
/*--------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>

#include <sys/thread.h>
#include <sys/timer.h>
#include <sys/version.h>
#include <sys/heap.h>


//for ir device
#include <dev/ir.h>
#include <dev/irsony.h>
#include <dev/irqreg.h>
	
#include "../include/system.h"
#include "../include/portio.h"
#include "../include/display.h"
#include "../include/led.h"
#include "../include/remcon.h"
#include "../include/keyboard.h"
#include "../include/log.h"
#include "../include/uart0driver.h"
#include "../include/mmc.h"
#include "../include/watchdog.h"
#include "../include/flash.h"
#include "../include/spidrv.h"
#include "../include/x1205.h"
#include "../include/menu.h"
#include "../include/handlebacklight.h"
#include "../include/vs10xx.h"

#include <time.h>
#include "../include/timedate.h"

#include "../include/FileIO.h"
#include "../include/rtc.h"
#include "../include/network.h"
#include "../include/ntp.h"

/*-------------------------------------------------------------------------*/
/* global variable definitions                                             */
/*-------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/
/* local variable definitions                                              */
/*-------------------------------------------------------------------------*/


/*-------------------------------------------------------------------------*/
/* local routines (prototyping)                                            */
/*-------------------------------------------------------------------------*/
static void SysMainBeatInterrupt(void*);
static void SysControlMainBeat(u_char);

/*-------------------------------------------------------------------------*/
/* Stack check variables placed in .noinit section                         */
/*-------------------------------------------------------------------------*/

/*!
 * \addtogroup System
 */

/*@{*/




/*-------------------------------------------------------------------------*/
/*                         start of code                                   */
/*-------------------------------------------------------------------------*/


/* ����������������������������������������������������������������������� */
/*!
 * \brief ISR MainBeat Timer Interrupt (Timer 2 for Mega128, Timer 0 for Mega256).
 *
 * This routine is automatically called during system
 * initialization.
 *
 * resolution of this Timer ISR is 4,448 msecs
 *
 * \param *p not used (might be used to pass parms from the ISR)
 */
/* ����������������������������������������������������������������������� */
static void SysMainBeatInterrupt(void *p)
{

    /*
     *  scan for valid keys AND check if a MMCard is inserted or removed
     */
    KbScan();
    CardCheckCard();
}


/* ����������������������������������������������������������������������� */
/*!
 * \brief Initialise Digital IO
 *  init inputs to '0', outputs to '1' (DDRxn='0' or '1')
 *
 *  Pull-ups are enabled when the pin is set to input (DDRxn='0') and then a '1'
 *  is written to the pin (PORTxn='1')
 */
/* ����������������������������������������������������������������������� */
void SysInitIO(void)
{
    /*
     *  Port B:     VS1011, MMC CS/WP, SPI
     *  output:     all, except b3 (SPI Master In)
     *  input:      SPI Master In
     *  pull-up:    none
     */
    outp(0xF7, DDRB);

    /*
     *  Port C:     Address bus
     */

    /*
     *  Port D:     LCD_data, Keypad Col 2 & Col 3, SDA & SCL (TWI)
     *  output:     Keyboard colums 2 & 3
     *  input:      LCD_data, SDA, SCL (TWI)
     *  pull-up:    LCD_data, SDA & SCL
     */
    outp(0x0C, DDRD);
    outp((inp(PORTD) & 0x0C) | 0xF3, PORTD);

    /*
     *  Port E:     CS Flash, VS1011 (DREQ), RTL8019, LCD BL/Enable, IR, USB Rx/Tx
     *  output:     CS Flash, LCD BL/Enable, USB Tx
     *  input:      VS1011 (DREQ), RTL8019, IR
     *  pull-up:    USB Rx
     */
    outp(0x8E, DDRE);
    outp((inp(PORTE) & 0x8E) | 0x01, PORTE);

    /*
     *  Port F:     Keyboard_Rows, JTAG-connector, LED, LCD RS/RW, MCC-detect
     *  output:     LCD RS/RW, LED
     *  input:      Keyboard_Rows, MCC-detect
     *  pull-up:    Keyboard_Rows, MCC-detect
     *  note:       Key row 0 & 1 are shared with JTAG TCK/TMS. Cannot be used concurrent
     */
#ifndef USE_JTAG
    sbi(JTAG_REG, JTD); // disable JTAG interface to be able to use all key-rows
    sbi(JTAG_REG, JTD); // do it 2 times - according to requirements ATMEGA128 datasheet: see page 256
#endif //USE_JTAG

    outp(0x0E, DDRF);
    outp((inp(PORTF) & 0x0E) | 0xF1, PORTF);

    /*
     *  Port G:     Keyboard_cols, Bus_control
     *  output:     Keyboard_cols
     *  input:      Bus Control (internal control)
     *  pull-up:    none
     */
    outp(0x18, DDRG);
}

/* ����������������������������������������������������������������������� */
/*!
 * \brief Starts or stops the 4.44 msec mainbeat of the system
 * \param OnOff indicates if the mainbeat needs to start or to stop
 */
/* ����������������������������������������������������������������������� */
static void SysControlMainBeat(u_char OnOff)
{
    int nError = 0;

    if (OnOff==ON)
    {
        nError = NutRegisterIrqHandler(&OVERFLOW_SIGNAL, SysMainBeatInterrupt, NULL);
        if (nError == 0)
        {
            init_8_bit_timer();
        }
    }
    else
    {
        // disable overflow interrupt
        disable_8_bit_timer_ovfl_int();
    }
}

/* ����������������������������������������������������������������������� */
/*!
 * \brief Main entry of the SIR firmware
 *
 * All the initialisations before entering the for(;;) loop are done BEFORE
 * the first key is ever pressed. So when entering the Setup (POWER + VOLMIN) some
 * initialisatons need to be done again when leaving the Setup because new values
 * might be current now
 *
 * \return \b never returns
 */
/* ����������������������������������������������������������������������� */
THREAD(stream, arg)
{
	NutThreadSetPriority(10);
	LogMsg_P(LOG_INFO, PSTR("Network thread created \n"));
	printf("Finish network init");
	NetworkInit();
	SetPlaying(1);
	
	for (;;)
	{	
		 if (playing == 0)
		 {
			//playing stream
			PlayStream();
		 }
		 if (playing == 1)
		 {
			 NutSleep(1000);
		 }
	}
}

THREAD(menuHandler, arg)
{
	NutThreadSetPriority(32);
	printf("Menu handler thread created");
	handleMenu();
}



int main(void)
{
	
	/* 
	 * Kroeske: time struct uit nut/os time.h (http://www.ethernut.de/api/time_8h-source.html)
	 *
	 */
	//tm gmt;
	/*
	 * Kroeske: Ook kan 'struct _tm gmt' Zie bovenstaande link
	 */
	
    /*
     *  First disable the watchdog
     */
    WatchDogDisable();

    NutDelay(100);

    SysInitIO();
	
	SPIinit();
    
	LedInit();
	
	LcdLowLevelInit();

    Uart0DriverInit();
    Uart0DriverStart();
	LogInit();
	LogMsg_P(LOG_INFO, PSTR("Hello World"));
    CardInit();
	
	
	
	/*
	 * Kroeske: sources in rtc.c en rtc.h
	 */
	x1205Init();			// Init x1205 chip
	x1205ReadByte(0x3f);	// Read x1205 STATUS register (0x3f)
	NutSleep(100);

	x1205Enable(); 			// Enable writing
	NutSleep(100);
	x1205ReadByte(0x3f);
	NutSleep(100);


	x1205WriteByte(0x37, 68);
	x1205ReadByte(0x3f);

    X12Init();
	
	
	//uncomment below to test automatically syncing:
	 // gmt.tm_sec = 50;
	 // gmt.tm_min = 59;
	 // gmt.tm_hour = 10;
	 // gmt.tm_mday = 31;
	 // gmt.tm_mon = 12;
	 // gmt.tm_year = 2016;
	 // gmt.tm_wday = 7;
	 // gmt.tm_yday = 366;
	 // gmt.tm_isdst = 0;
	
	 // setTime(&gmt); 
	
	//setAlarm(Alarm1,11,0,					 Alarm_Status_On,0);	
	//setScheduler(Scheduler1, 11, 00, 0, 1, Alarm_Status_On, 0);
	//setfallasleep(fallasleep1, 11, 0, 0, 5, Alarm_Status_On, 0);

    if (At45dbInit()==AT45DB041B)
    {
        // ......
    }

    RcInit();
	KbInit();

    SysControlMainBeat(ON);             // enable 4.4 msecs hartbeat interrupt
	VsPlayerInit();
	
	//Load user configuration from volatile memory
	loadUserConfig();
	
    /*
     * Increase our priority so we can feed the watchdog.
     */
    //NutThreadSetPriority(1);

	/* Enable global interrupts */
	sei();
	
	//start thread for handling backlight
	startbackgroundhandling();

	tm td;
	getTime(&td);

	printf("Time & Date loaded...  value: %02d:%02d %02d-%02d-%d", td.tm_hour, td.tm_min, td.tm_mday, td.tm_mon, td.tm_year);

	NutThreadCreate("menu", menuHandler, NULL, 512);
	
	//set thread for playing stream
	NutThreadCreate("bg", stream, NULL, 512);

	LogMsg_P(LOG_INFO, PSTR("Heap value: %d"),NutHeapAvailable());

    for (;;)
    {	
		NutSleep(100);
    }
    return(0);      // never reached, but 'main()' returns a non-void, so.....
}
/* ---------- end of module ------------------------------------------------ */

/*@}*/
