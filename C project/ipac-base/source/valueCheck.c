#include "valueCheck.h"

int checkValue(int value, int max, int min)
{
	if (value < min)
	{
		value = min;
	}
	if (value > max)
	{
		value = max;
	}
	return value;
}

int mutateValue(int value, int mutate, int max, int min)
{	
	value += mutate;
	return checkValue(value, max, min);
}

/*
Deze functie gebruikt 2 functie points.
Eerste functie pointer is om de value op te halen.
Tweede functie pointer om de value weer op te slaan(setten).
mutate value geeft aan hoeveel je erbij op wilt tellen,
dus als je eraf wilt halen moet het getal negatief zijn.
Max: de maximale value die het getal mag zijn.
Min: de minimale value die het getal mag zijn.
*/
void mutateValueWithF(int(*getValue)(void), void(*setValue)(int), int mutate, int max, int min)
{
	int value = (*getValue)();
	
	(*setValue)(mutateValue(value,mutate,max,min));
}
