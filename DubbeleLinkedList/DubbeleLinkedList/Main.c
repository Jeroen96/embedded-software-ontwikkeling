
#include <stdio.h>

struct node {
	int	number;
	struct node * previous;
	struct node * next;
};

struct node* buildList() {
	struct node* head = NULL;
	struct node* second = NULL;
	struct node* third = NULL;

	head = malloc(sizeof(struct node)); 
	second = malloc(sizeof(struct node));
	third = malloc(sizeof(struct node));

	head->number = -1; 
	head->next = second;
	head->previous = NULL;

	second->number = 0; 
	second->next = third;
	second->previous = head;

	third->number = 1; 
	third->next = NULL;
	third->previous = second;
	

	return head;
}

int length(struct node* head) {
	struct node* current = head;
	int count = 0;

	while (current != NULL) {
		count++;
		current = current->next;
	}
	return count;
}

void add(struct node* head, int n) {
	struct node* current = head;
	struct node* newNumber = NULL;

	newNumber = malloc(sizeof(struct node));

	while (current->next != NULL) {
		current = current->next;
	}

	newNumber->number = n;
	newNumber->previous = current;
	newNumber->next = NULL;
	current->next = newNumber;
}

void removeItem(struct node* head, struct node* rem) {
	struct node* p = NULL;
	struct node* n = NULL;
	struct node* current = head;
	struct node* remove = NULL;

	while (current != NULL) {
		if (current == rem) {
			if (current->previous != NULL) {
				p = current->previous;
			}

			if (current->next != NULL) {
				n = current->next;
			}
			remove = current;
			

			if (n != NULL) {
				n->previous = p;
				current = n;
			}

			if (p != NULL) {
				p->next = n;
				current = p;
			}
			free(remove);
		}
		current = current->next;
	}
}

int exists(struct node* head, struct node* item) {
	struct node* current = head;

	while (current != NULL) {
		if (current == item) {
			return 1;
		}
		current = current->next;
	}
	return 0;
}

void Clear(struct node* head) {
	struct node* current = head;
	struct node* remove = NULL;

	while (current != NULL) {
		remove = current;
		current = current->next;

		free(remove);
	}
}

void main() {
	char cmd[256];
	struct node* head = buildList();
	struct node* current = head;

	add(head, 2);
	printf("length is %d \n", length(head));

	while (1) {
		gets(cmd);
		if (cmd[0] == 's') {
			if (current->previous != NULL) {
				current = current->previous;
			}
		}
		else if (cmd[0] == 'w'){
			if (current->next != NULL) {
				current = current->next;
			}
		}
		else if (cmd[0] == 'd') {
			if (current->previous != NULL) {
				struct node* delete = current;
				current = current->previous;
				removeItem(head, delete);
			}
			else if (current->next != NULL) {
				struct node* delete = current;
				current = current->next;
				removeItem(head, delete);
			}
			else {
				printf("Cant delete, no other item available!\n");
			}
		}
		else if (cmd[0] == 'e') {
			if (exists(head,current) == 1) {
				printf("item exists!\n");
			}
			else {
				printf("Item doesnt exist!\n");
			}
		}
		else if (cmd[0] == 'c') {
			Clear(head);
			printf("List cleared! \n");
		}
		printf("number is %d \n", current->number);
	}
}
































































